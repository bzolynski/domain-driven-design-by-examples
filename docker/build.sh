#!/usr/bin/env bash
docker build -t domain_driven_design/php images/php
docker build -t domain_driven_design/nginx images/nginx
docker build -t domain_driven_design/mysql images/mysql
