all : composer
.PHONY: all start stop composer phpunit db-drop db-create db-fixtures

start :
	docker-compose up -d

stop :
	docker-compose down

composer :
	docker-compose run --rm --no-deps php composer install

composer-require :
	docker-compose run --rm --no-deps php composer require $(package)

composer-require-dev :
	docker-compose run --rm --no-deps php composer require --dev $(package)

composer-remove-dev :
	docker-compose run --rm --no-deps php composer remove --dev $(package)

phpunit :
	docker-compose run --rm --no-deps php vendor/bin/phpunit --testsuite DomainLogic

phpunit-coverage-text :
	docker-compose run --rm --no-deps php vendor/bin/phpunit --testsuite DomainLogic --coverage-text

phpunit-coverage-html :
	docker-compose run --rm --no-deps php vendor/bin/phpunit --testsuite DomainLogic --coverage-html coverage/

db-drop :
	docker-compose run --rm --no-deps php php doctrine.php orm:schema-tool:drop --force

db-create :
	docker-compose run --rm --no-deps php php doctrine.php orm:schema-tool:create

db-update :
	docker-compose run --rm --no-deps php php doctrine.php orm:schema-tool:update --force

db-fixtures :
	docker-compose run --rm --no-deps php php doctrine.php doctrine:fixtures:load

behat-all :
	docker-compose exec php sh -c "./vendor/bin/behat --config behat/config.yml"

behat-tags :
	docker-compose exec php sh -c "./vendor/bin/behat --config behat/config.yml --tags $(tags)"
