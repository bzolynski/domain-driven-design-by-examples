<?php

declare(strict_types=1);

namespace ValueObject\Test;

use PHPUnit\Framework\TestCase;
use ValueObject\Weight;

class WeightTest extends TestCase
{
    public function testValidIntegerInputWeight()
    {
        $weight = new Weight(100);
        $this->assertSame(100.0, $weight->getValue());
        $this->assertTrue(is_float($weight->getValue()));
    }

    public function testValidFloatInputWeight()
    {
        $weight = new Weight(100.98);
        $this->assertSame(100.98, $weight->getValue());
        $this->assertTrue(is_float($weight->getValue()));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidWeight()
    {
        new Weight(-100);
    }
}
