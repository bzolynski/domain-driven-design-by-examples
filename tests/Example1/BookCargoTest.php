<?php

declare(strict_types=1);

namespace DomainLogic\Test;

use PHPUnit\Framework\TestCase;
use Example1\Model\Cargo;
use Example1\Model\Ship;
use ValueObject\Weight;

class BookCargoTest extends TestCase
{
    public function testBookCargo()
    {
        $ship = new Ship(new Weight(500));

        $cargo = new Cargo(new Weight(200));
        $ship->bookCargo($cargo);

        $cargo = new Cargo(new Weight(300));
        $ship->bookCargo($cargo);

        $this->assertSame($ship->getBookedWeight(), 500.0);
    }

    /**
     * @expectedException \OverflowException
     */
    public function testBookCargoExceeded()
    {
        $ship = new Ship(new Weight(500));

        $cargo = new Cargo(new Weight(200));
        $ship->bookCargo($cargo);

        $cargo = new Cargo(new Weight(600));
        $ship->bookCargo($cargo);
    }
}
