<?php

declare(strict_types=1);

namespace DomainLogic\Test;

use Example1\Policy\ShipWeightPolicy;
use PHPUnit\Framework\TestCase;
use Example1\Model\Cargo;
use Example1\Model\Ship;
use ValueObject\Weight;

class ShipWeightPolicyTest extends TestCase
{
    public function testMaxCargoOverweightPercentageValue()
    {
        $this->assertSame(0.1, ShipWeightPolicy::MAX_CARGO_OVERWEIGHT_PERCENTAGE);
    }

    public function testPolicyFulfilled()
    {
        $ship = new Ship(new Weight(200));
        $cargo = new Cargo(new Weight(200 + 200 * 0.0999));
        $this->assertTrue(ShipWeightPolicy::canCargoBeBooked($ship, $cargo));
    }

    public function testPolicyFulfilledForMaximumOverweight()
    {
        $ship = new Ship(new Weight(200));
        $cargo = new Cargo(new Weight(200 + 200 * 0.1));
        $this->assertTrue(ShipWeightPolicy::canCargoBeBooked($ship, $cargo));
    }

    public function testPolicyUnfulfilled()
    {
        $ship = new Ship(new Weight(200));
        $cargo = new Cargo(new Weight(200 + 200 * 0.1001));
        $this->assertFalse(ShipWeightPolicy::canCargoBeBooked($ship, $cargo));
    }
}
