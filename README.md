# Domain Driven Design by Examples

This is Domain Driven Design examples while solving domain based problems.


# Technical specification

- PHP 7 with nginx
- Docker (Docker Compose used)
- Doctrine 2 ORM (MySQL)
- Doctrine data fixtures
- PHPUnit for unit tests
- Behat for BDD specification and tests
- Monolog logger
- YAML Reader
- Incenteev parameter handler
- Symfony Command
- Timestampable (Gedmo) Doctrine annotation for created date fields


# Installation

For development environment, Docker has been used with _docker-compose_. 
It requires images for nginx, PHP and MySQL (`Dockerfile`s are included in "docker/" directory).

## Notes before installation:

- Code will be attached as the separate data container.
- HTTP is exposed from container on port 80, and MySQL on port 3306.
- Because MySQL container is linked to PHP container, MySQL is resolved by "mysql" hostname for DB connection.
- Use default parameters for MySQL connection, and if not, set the same values in `parameters.yml` and in `docker-compose.yml` files.
- MySQL database will be persisted to `database/` folder and attached as the volume to `/var/lib/mysql`
- All logs from Monolog will be written as relative path to "logs/" directory which is configurable in `parameters.yml`.

## Installation process:

1. Install Docker engine (at least in version 1.10.0) and _docker-compose_ (at least in version 1.7.0 as `exec` command has been used).
2. Build containers by executing: `docker/build.sh` bash command.
3. Run `docker-compose up` or `make start` in main directory which reads `docker-compose.yml` file.
4. Install composer dependencies by running `make composer` (you will be asked for parameters, just choose defaults).
5. Load database fixtures if you want to test persistence layer (not implemented yet):
    - `make db-create`
    - `make db-fixtures`


# Running

1. `make start` to run Docker containers.
2. `make stop` to stop running Docker.
3. `docker-compose logs` to see logs from containers.


# Testing
  
    make phpunit
    make phpunit-coverage-text
    make phpunit-coverage-html


# ORM

## Dropping database schema
  
    make db-drop

## Creating database schema
  
    make db-create

## Update database schema
  
    make db-update

## Loading data fixtures to database
  
    make db-fixtures
