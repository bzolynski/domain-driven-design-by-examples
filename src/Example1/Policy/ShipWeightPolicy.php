<?php

declare(strict_types=1);

namespace Example1\Policy;

use Example1\Model\Ship;
use Example1\Model\Cargo;

class ShipWeightPolicy
{
    const MAX_CARGO_OVERWEIGHT_PERCENTAGE = 10 / 100;

    public static function canCargoBeBooked(Ship $ship, Cargo $cargoToAdd): bool
    {
        $allowedOverbookedWeight = self::getAllowedOverbookedWeight($ship);
        if ($ship->getBookedWeight() + $cargoToAdd->getWeight() > $allowedOverbookedWeight) {
            return false;
        }
        return true;
    }

    private static function getAllowedOverbookedWeight(Ship $ship): float
    {
        $maxCargoWeight = $ship->getMaximumCargoWeight();
        return $maxCargoWeight + $maxCargoWeight * self::MAX_CARGO_OVERWEIGHT_PERCENTAGE;
    }
}
