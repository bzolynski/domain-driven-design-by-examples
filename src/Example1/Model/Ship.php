<?php

declare(strict_types=1);

namespace Example1\Model;

use OverflowException;
use ValueObject\Weight;
use Example1\Policy\ShipWeightPolicy;

class Ship
{
    private $bookedWeight = 0;

    private $maximumCargoWeight;

    public function __construct(Weight $maximumCargoWeight)
    {
        $this->maximumCargoWeight = $maximumCargoWeight->getValue();
    }

    public function bookCargo(Cargo $cargo)
    {
        if (!ShipWeightPolicy::canCargoBeBooked($this, $cargo)) {
            throw new OverflowException('Cargo cannot be booked to ship because it is too heavy');
        }
        $this->bookedWeight += $cargo->getWeight();
    }

    public function getBookedWeight(): float
    {
        return $this->bookedWeight;
    }

    public function getMaximumCargoWeight(): float
    {
        return $this->maximumCargoWeight;
    }
}
