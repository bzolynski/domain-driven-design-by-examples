<?php

declare(strict_types=1);

namespace Example1\Model;

use ValueObject\Weight;

class Cargo
{
    private $weight;

    public function __construct(Weight $weight)
    {
        $this->weight = $weight;
    }

    public function getWeight(): float
    {
        return $this->weight->getValue();
    }
}
