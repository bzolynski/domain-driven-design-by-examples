<?php

declare(strict_types=1);

namespace ValueObject;

use InvalidArgumentException;

class Weight
{
    private $value;

    public function __construct(float $value)
    {
        $this->validate($value);
        $this->value = $value;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    private function validate(float $value)
    {
        if ($value < 0) {
            throw new InvalidArgumentException('Value cannot be negative');
        }
    }
}
